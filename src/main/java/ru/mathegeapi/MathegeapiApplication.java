package ru.mathegeapi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MathegeapiApplication {

	public static void main(String[] args) {
		SpringApplication.run(MathegeapiApplication.class, args);
	}

}
